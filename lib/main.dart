import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  State<HelloFlutterApp> createState() => _HelloFlutterAppState();
}

String eng = "Hello Flutter";
String spain = "Hola Flutter";
String thai = "สวัสดี ฟลัสเตอร์";
String finland = "Hei Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = eng;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        //Scaffold Widget
        home: Scaffold(
          appBar: AppBar(
            title: Text("Hello Flutter"),
            leading: Icon(Icons.home),
            actions: <Widget>[
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == eng ? spain : eng;
                    });
                  },
                  icon: Icon(Icons.access_alarm)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == eng ? thai : eng;
                    });
                  },
                  icon: Icon(Icons.abc_outlined)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == eng ? finland : eng;
                    });
                  },
                  icon: Icon(Icons.cabin))
            ],
          ),
          body: Center(
            child: Text(
              "$displayText",
              style: TextStyle(fontSize: 24),
            ),
          ),
        ));
  }
}

// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         debugShowCheckedModeBanner: false,
//         //Scaffold Widget
//         home: Scaffold(
//           appBar: AppBar(
//             title: Text("Hello Flutter"),
//             leading: Icon(Icons.home),
//             actions: <Widget>[
//               IconButton(onPressed: () {}, icon: Icon(Icons.refresh))
//             ],
//           ),
//           body: Center(
//             child: Text(
//               "Hello Flutter",
//               style: TextStyle(fontSize: 24),
//             ),
//           ),
//         ));
//   }
// }
